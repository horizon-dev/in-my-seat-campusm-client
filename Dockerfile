FROM node:10.12.0-jessie

ARG UID
ARG GID

RUN npm install -g https://npm.campusm.net/get/aek-cli

RUN usermod -u ${UID} node \
    && groupmod -og ${GID} node \
    && mkdir /projects \
    && chown node:node /projects

USER node:node

WORKDIR /projects
