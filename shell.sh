#!/bin/sh

export _UID=$(id -u)
export _GID=$(id -g)
docker-compose build --build-arg UID=$_UID --build-arg GID=$_GID campusm
docker-compose run --rm --service-ports campusm /bin/bash
