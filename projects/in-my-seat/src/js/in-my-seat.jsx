import React from "react";

import reactRender from "@ombiel/aek-lib/react/utils/react-render";
import * as _ from "@ombiel/aek-lib/utils";

import GeoLocation from "@ombiel/aek-lib/client-tools/geolocation";

import {
  KmlLayer,
  Marker,
  SearchBox
} from "react-google-maps";

import { Map } from "./in-my-seat/map";
import * as api from "./in-my-seat/api.es6";


class Screen extends React.Component {

  constructor(props) {
    super(props);
    let self = this;
    this.props = props;
    this.state = {
      center: { lat: 52.9375237, lng: -1.1967676 },
      position: { lat: 52.9375237, lng: -1.1967676 },
      stops: [],
      liveBuses: []
    };
    this.recenter = this.recenter.bind(this);
    this.updateStops = this.updateStops.bind(this);
    this.updateLiveBuses = this.updateLiveBuses.bind(this);
    this.onMapMounted = this.onMapMounted.bind(this);
    this.mapChanged = ((function(){
      let timer;
      return function() {
          clearTimeout(timer);
          timer = setTimeout(function() {
            api.locGeoPos(
              self.state.center.lat,
              self.state.center.lng).then(self.updateStops);
          }, 500);
      };
    })()).bind(this);
    let geoloc = new GeoLocation({watch: true});
    geoloc.on('update', function(pos) {
      self.setState(Object.assign(self.state, {
        position: {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude
        }
      }));
    });
  }

  componentWillMount() {
    this.refs = {};
  }

  componentDidMount() {
    let self = this;
    new GeoLocation({watch: false}).on('update', pos =>{
      this.setState(Object.assign(this.state, {
        position: {
          lat: pos.coords.latitude,
          lng: pos.coords.longitude
        }
      }));
      this.recenter();
    });
    setInterval(function(){
      if ("map" in self.refs) {
        let bounds = self.refs.map.getBounds();
        if (bounds) {
          api.journeyGeoPos(
            bounds.getNorthEast().lat(),
            bounds.getNorthEast().lng(),
            bounds.getSouthWest().lat(),
            bounds.getSouthWest().lng()
          ).then(self.updateLiveBuses);
        }
      }
    }, 10000);
  }

  render() {

    let stops = this.state.stops.map(function(s, i) {
      let lat = s.crd.y / (10 ** 6);
      let lng = s.crd.x / (10 ** 6);
      return <Marker
        key={i}
        icon={_.publicPath("/images/icon-bus-stop-64.png")}
        position={{"lat": lat, "lng": lng}}
      />;
    });

    let liveBuses = this.state.liveBuses.map(function(b, i) {
      let lat = b.pos.y / (10 ** 6);
      let lng = b.pos.x / (10 ** 6);
      return <Marker
        key={i}
        icon={_.publicPath("/images/icon-bus-32.png")}
        position={{"lat": lat, "lng": lng}}
      />;
    });

    return (
      <div style={{height: `100%`}}>
        <Map
            key="map"
            position={this.state.position}
            center={this.state.center}
            onCenterChanged={this.mapChanged}
            onBoundsChanged={this.mapChanged}
            onMapMounted={this.onMapMounted}>
          { stops }
          { liveBuses }
          {/*<SearchBox
              controlPosition={1}
              onPlacesChanged={this.onPlacesChanged}>
            <input
                type="text"
                placeholder="Search..."
                style={{
                  boxSizing: `border-box`,
                  border: `1px solid transparent`,
                  width: `75%`,
                  height: `32px`,
                  marginTop: `5px`,
                  padding: `0 12px`,
                  borderRadius: `3px`,
                  boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                  fontSize: `14px`,
                  outline: `none`,
                  textOverflow: `ellipses`
                }}
            />
          </SearchBox>*/}
          {/*<KmlLayer
            url="https://services.wp.horizon.ac.uk/ims/facticles.kmz"
            options={{ preserveViewport: true }}
          />*/}
        </Map>
        <nav className="topnav">
          <img
              className="controlicon"
              src={_.publicPath("/images/icon-target.png")}
              onClick={this.recenter} />
        </nav>
      </div>
    );
  }

  recenter() {
    this.setState(Object.assign(this.state, {
      center: this.state.position
    }));
  }

  updateStops(res) {
    this.setState(Object.assign(this.state, {
      stops: res.body.svcResL[0].res.locL
    }));
  }

  updateLiveBuses(res) {
    this.setState(Object.assign(this.state, {
      liveBuses: res.body.svcResL[0].res.jnyL
    }));
  }

  onMapMounted(ref) {
    this.refs.map = ref;
  }

}

reactRender(<Screen />);

/*let Message = require("@ombiel/aek-lib/react/components/message");
let Header = require("@ombiel/aek-lib/react/components/header");
class TestScreen extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    const { children } = this.props;
    return <Message>{children}<p>My Message</p></Message>;
  }

}
reactRender(<TestScreen>
  <Header level="2" subtext="Sub Header" dividing>
    My Header
  </Header>
</TestScreen>);*/
