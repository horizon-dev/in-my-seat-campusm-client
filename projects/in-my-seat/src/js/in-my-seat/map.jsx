import React from "react";

import {
  compose,
  withProps
} from "recompose";

import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

import * as _ from "@ombiel/aek-lib/utils";

import * as settings from "./settings";

const MapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key="
        + settings.GOOGLE_API_KEY + "&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100%` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap
)((props) => (
  <GoogleMap
      zoom={15}
      center={props.center}
      onCenterChanged={props.onCenterChanged}
      onBoundsChanged={props.onBoundsChanged}
      ref={props.onMapMounted}
      options={{
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: false
      }} >
    <Marker
        icon={_.publicPath("/images/mylocation.gif")}
        position={props.position}
    />

    {props.children}
  </GoogleMap>
));

/**
 * A map with a marker defining current location.
 */
export class Map extends React.PureComponent {

  constructor(props) {
    super(props);
  }

  render() {
    const { children } = this.props;
    return React.createElement(MapComponent, this.props, children);
  }

}
