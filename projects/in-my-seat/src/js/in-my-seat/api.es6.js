import request from "@ombiel/aek-lib/request";
import * as settings from "./settings";

const base_request = {
  "auth": {
    "aid": "a4r2b3i8x2n9f6l0",
    "type": "AID"
  },
  "client": {
    "id": "ARRIVA",
    "name": "Arriva Bus ",
    "os": "Android 6.0.1",
    "res": "1024x768",
    "type": "AND",
    "ua": "Dalvik/2.1.0 (Linux; U; Android 6.0.1; Nexus 5 Build/M4B30Z)",
    "v": 1000038
  },
  "formatted": false,
  "lang": "eng",
  "ver": "1.16"
};

function toArrivaCoord(c) {
  return parseInt(c * (10**6));
}

export function locGeoPos(latitude, longitude) {
  let req = {
    "svcReqL": [{
      "cfg": {
        "polyEnc": "GPA"
      },
      "meth": "LocGeoPos",
      "req": {
        "getPOIs": false,
        "maxLoc": 2,
        "ring": {
          "cCrd": {
            "x": toArrivaCoord(longitude),
            "y": toArrivaCoord(latitude)
          }
        }
      }
    }]
  };
  Object.assign(req, base_request);
  return request
    .post(settings.PROXY_URL)
    .send(req);
}

export function journeyGeoPos(north, east, south, west) {
  let req = {
    "svcReqL": [
      {
        "cfg": {
          "polyEnc": "GPA"
        },
        "meth": "JourneyGeoPos",
        "req": {
          "jnyFltrL": [
            {
              "mode": "BIT",
              "type": "PROD",
              "value": "000001"
            }
          ],
          "maxJny": 128,
          "onlyRT": true,
          "perSize": 30000,
          "rect": {
            "llCrd": {
              "x": toArrivaCoord(west),
              "y": toArrivaCoord(south)
            },
            "urCrd": {
              "x": toArrivaCoord(east),
              "y": toArrivaCoord(north)
            }
          },
          "trainPosMode": "REPORT_ONLY"
        }
      }
    ]
  };
  Object.assign(req, base_request);
  return request
    .post(settings.PROXY_URL)
    .send(req);
}
